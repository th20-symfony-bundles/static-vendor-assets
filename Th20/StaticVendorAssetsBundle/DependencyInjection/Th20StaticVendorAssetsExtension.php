<?php

namespace Th20\StaticVendorAssetsBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

use Th20\StaticVendorAssetsBundle\Model\AssetsScanner;
use Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


/**
 * Loads and manages bundle configuration.
 */
class Th20StaticVendorAssetsExtension extends Extension implements PrependExtensionInterface
{

    public function load(array $configs, ContainerBuilder $container)
    {
        // @see prepend()
    }

    public function prepend(ContainerBuilder $container)
    {
        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $configs);

        $bundles = $container->getParameter('kernel.bundles');
        if (isset($bundles['AsseticBundle'])) {
            $this->configureAsseticBundle($container, $config);
        }
    }


    protected function configureAsseticBundle(ContainerBuilder $container, $config)
    {
        $assets = $this->getStaticAssets($container, $config['vendors']);
        foreach ($container->getExtensions() as $name => $extension) {
            if ($name == 'assetic' && !empty($assets)) {
                $container->prependExtensionConfig($name, array('assets' => $assets));
            }
        }
    }

    protected function getStaticAssets(ContainerBuilder $container, $config)
    {
        $vendorsDir = $this->getVendorDir($container);
        $assets = array();

        foreach ($config as $vendor => $rules) {
            if (isset($rules['css'])) {
                $builder = new RulesBuilder\CssRulesBuilder($vendorsDir, $vendor);
                $assets += $builder->scanVendorDir($rules['css']);
                unset($rules['css']);
            }

            if (isset($rules['js'])) {
                $builder = new RulesBuilder\JsRulesBuilder($vendorsDir, $vendor);
                $assets += $builder->scanVendorDir($rules['js']);
                unset($rules['js']);
            }

            foreach ($rules as $destination => $files) {
                if (empty($destination)) {
                    $builder = new RulesBuilder\DefaultRulesBuilder($vendorsDir, $vendor);
                } elseif (substr($destination, -1) == '/') {
                    $builder = new RulesBuilder\PrefixRulesBuilder($vendorsDir, $vendor);
                } else {
                    $builder = new RulesBuilder\FixedRulesBuilder($vendorsDir, $vendor);
                }

                $assets += $builder->scanVendorDir($files, $destination);
            }
        }

        return $assets;
    }

    protected function getVendorDir(ContainerBuilder $container)
    {
        return realpath($container->getParameter('kernel.root_dir') . '/../vendor/');
    }

}
