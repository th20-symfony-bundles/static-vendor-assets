<?php

namespace Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


use Th20\StaticVendorAssetsBundle\Model\AssetsScanner;


abstract class AbstractRulesBuilder
{

    protected $vendorsDir;

    protected $vendor;


    abstract public function scanVendorDir(array $rules, $destination = null);


    public function __construct($vendorsDir, $vendor)
    {
        if (substr($vendorsDir, - 1) != '/') {
            $vendorsDir .= '/';
        }
        $this->vendorsDir = $vendorsDir;

        $this->vendor = trim($vendor);
    }

    protected function stripVendorPrefix($path)
    {
        return str_replace($this->vendorsDir, '', $path);
    }

    protected function splitAtRulePrefix($path, $prefix)
    {
        if (strpos($path, $prefix) !== false) {
            $parts = explode($prefix, $path, 2);
            return $parts[1];
        }

        return $path;
    }

    protected function selectScannedFiles($files, $regexp)
    {
        $match = array();
        $notMatch = array();

        foreach ($files as $file) {
            if (preg_match($regexp, $file)) {
                $match[] = $file;
            } else {
                $notMatch[] = $file;
            }
        }

        return array($match, $notMatch);
    }

    protected function createAssetsScanner()
    {
        return new AssetsScanner($this->vendorsDir, $this->vendor);
    }

    protected function clearAssetName($name)
    {
        return preg_replace('/[^a-z0-9_]/', '_', $name);
    }

}
