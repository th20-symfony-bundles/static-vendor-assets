<?php

namespace Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


class PrefixRulesBuilder extends AbstractRulesBuilder
{

    public function scanVendorDir(array $rules, $destination = null)
    {
        if (substr($destination, -1) != '/') {
            $destination .= '/';
        }

        $assets = array();
        $vendor = $this->vendor;

        $scanner = $this->createAssetsScanner();

        foreach ($rules as $rule) {
            $scanned = $scanner->scan(array($rule));

            foreach ($scanned as $file) {
                $target = $this->stripVendorPrefix($file);
                $target = $this->splitAtRulePrefix($target, $rule);
                $target = $destination . $target;

                $hash = substr(md5($file), 0, 7);
                $assets[$this->clearAssetName("static_{$vendor}_{$hash}")] = array(
                    'inputs' => $file,
                    'output' => $target,
                    'filters' => array(),
                );
            }
        }

        return $assets;
    }
}
