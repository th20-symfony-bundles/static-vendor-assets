<?php

namespace Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


class JsRulesBuilder extends AbstractRulesBuilder
{

    public function scanVendorDir(array $rules, $destination = null)
    {
        $assets = array();
        $vendor = $this->vendor;

        $scanner = $this->createAssetsScanner();
        $scanned = $scanner->scan($rules);

        list($matched, $scanned) = $this->selectScannedFiles($scanned, '/\.js$/');
        if ($matched) {
            $assets[$this->clearAssetName("static_{$vendor}_js")] = array(
                'inputs' => $matched,
                'output' => "js/$vendor.js",
                'filters' => array(),
            );
        }

        return $assets;
    }

}
