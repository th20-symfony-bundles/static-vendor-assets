<?php

namespace Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


class FixedRulesBuilder extends AbstractRulesBuilder
{

    public function scanVendorDir(array $rules, $destination = null)
    {
        if (empty($destination)) {
            return array();
        }

        $assets = array();
        $vendor = $this->vendor;

        $scanner = $this->createAssetsScanner();
        $scanned = $scanner->scan($rules);

        $hash = str_replace('/', '_', $destination);
        $assets[$this->clearAssetName("static_{$vendor}_{$hash}")] = array(
            'inputs' => $scanned,
            'output' => $destination,
            'filters' => array(),
        );

        return $assets;
    }

}
