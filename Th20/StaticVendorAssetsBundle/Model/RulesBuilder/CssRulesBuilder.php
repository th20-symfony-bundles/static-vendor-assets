<?php

namespace Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


class CssRulesBuilder extends AbstractRulesBuilder
{

    public function scanVendorDir(array $rules, $destination = null)
    {
        $assets = array();
        $vendor = $this->vendor;

        $scanner = $this->createAssetsScanner();
        $scanned = $scanner->scan($rules);

        list($matched, $scanned) = $this->selectScannedFiles($scanned, '/\.css$/');
        if ($matched) {
            $assets[$this->clearAssetName("static_{$vendor}_css")] = array(
                'inputs' => $matched,
                'output' => "css/$vendor.css",
                'filters' => array(),
            );
        }

        return $assets;
    }

}
