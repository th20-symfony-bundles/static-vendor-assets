<?php

namespace Th20\StaticVendorAssetsBundle\Model\RulesBuilder;


class DefaultRulesBuilder extends AbstractRulesBuilder
{

    public function scanVendorDir(array $rules, $destination = null)
    {
        $assets = array();
        $vendor = $this->vendor;

        $scanner = $this->createAssetsScanner();
        $scanned = $scanner->scan($rules);

        foreach ($scanned as $file) {
            $hash = substr(md5($file), 0, 7);
            $target = 'bundles/static-vendor/' . $this->stripVendorPrefix($file);

            $assets[$this->clearAssetName("static_{$vendor}_{$hash}")] = array(
                'inputs' => $file,
                'output' => $target,
                'filters' => array(),
            );
        }

        return $assets;
    }
}
