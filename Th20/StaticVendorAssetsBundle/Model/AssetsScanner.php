<?php

namespace Th20\StaticVendorAssetsBundle\Model;


use InvalidArgumentException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;


class AssetsScanner
{

    protected $vendorDir;

    protected $vendorName;

    protected $scanDir;


    public function __construct($vendorDir, $vendorName)
    {
        $this->vendorDir = $vendorDir;
        $this->vendorName = $vendorName;

        $this->scanDir = "$vendorDir/$vendorName";
        if (!is_dir($this->scanDir) || !is_readable($this->scanDir)) {
            throw new InvalidArgumentException('Vendor directory does not exist or is not readable: ' . $this->scanDir);
        }
    }

    public function scan(array $rules)
    {
        if (empty($rules)) {
            $rules = $this->defaultScanRules();
        }
        $scanned = array();

        $dir = new RecursiveDirectoryIterator($this->scanDir);
        $files = new RecursiveIteratorIterator($dir);

        foreach ($rules as $rule) {
            if (!$this->ruleIsRegexp($rule)) {
                $rule = $this->convertRuleToRegexp($rule);
            }
            $filtered = new RegexIterator($files, $rule, RegexIterator::MATCH);
            foreach ($filtered as $file) {
                $path = $file->getPathName();
                if (is_file($path)) {
                    $scanned[$path] = $path;
                }
            }
        }

        return $scanned;
    }

    public function defaultScanRules()
    {
        return array(
            '/\.js$/', '/\.css$/',
        );
    }

    protected function ruleIsRegexp($rule)
    {
        return $rule[0] == '/' && $rule[strlen($rule) - 1] == '/';
    }

    protected function convertRuleToRegexp($rule)
    {
        return '/' . preg_quote($rule, '/') . '/';
    }

}
